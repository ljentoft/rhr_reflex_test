/**************************************************************************/
/*!
 @file Encoder_Driver.ino
 @author Ken Malsky
 @license Proprietary (c) 2014 Right Hand Robotics
 
 This is an example driver for the AMS AS5048A rotary encoder
 
 @section HISTORY
 
 v2.0 - Working for daisy chained encoders
 v1.0 - Working for a single encoder
 
 @section NOTES
 
 This may be used with one or more encoders in an SPI daisy chain configuration.
 (i.e. MOSI on the master connects to MOSI on the first in the chain. Subsequnt
 encoders connect MISO out to MOSI in on the next in the chain with the last
 returning to MISO on the master.) Set the constant NUM_ENCODERS to 1 for use
 with a single device.
 
 Connections for single encoder:
 Arduino       AS5048A    6-pin Cable
 MISO          MISO       Pin 1 - MISO
 MOSI          MOSI       Pin 2 - MOSI
 SCK           CLK        Pin 3 - SCLK
 3.3V          VDD3V      Pin 4 - 3.3V
 GND           GND        Pin 5 - GND
 D4            CSn        Pin 6 - NSS

For multiple encoders:
 * Connect first encoder MOSI to controller MOSI
 * Connect MISO to MOSI in daisy chain
 * Connect last encoder in chain MISO to controller MISO
 
 ***** NOTE: Data comes out in reverse order (last encoder first)             *****
 *****       Increasing values are counterclockwise facing the top of the IC  *****

 */

// Set number of encoders in daisy chain
int NUM_ENCODERS = 1;

// Arduino data pin used for chip select
const int chipSelectPin = 4;
const int LEDPin = 13;


#include <SPI.h>

const unsigned short ENCODER_VALUE_MASK = 0x3FFF;
const unsigned short ENC_ERROR_FLAG = 0x0400;

const unsigned short ENCODER_ANGLE = 0x3FFF;
const unsigned short ENCODER_GAIN = 0x3FFE;



void setup()
{

// initialize the console
  Serial.begin(9600);

  // initalize the SPI interface
  pinMode(chipSelectPin, OUTPUT);
  SPI.begin();
  SPI.setBitOrder(MSBFIRST);
  SPI.setDataMode(SPI_MODE1);
  SPI.setClockDivider(SPI_CLOCK_DIV4);  // SPI_CLOCK_DIV4 = 4MHz, supports SPI_CLOCK_DIV2 = 8MHz

  // initialize the chip select to deasserted
  digitalWrite(chipSelectPin, HIGH);
}

void loop()
{
  // Read the angle from the encoder
  ReadEncoders(NUM_ENCODERS, ENCODER_ANGLE);

/*
  // Read magnetic field strength for debug
  ReadEncoders(NUM_ENCODERS, ENCODER_GAIN);
  Serial.print("\n");
*/

  // delay is added to the loop to allow the serial console to keep up
  // *** remove this for real world use ***
  delay(25);
}


// Read from the SPI port
void ReadEncoders(int numEncoders, unsigned short theRegister)
{
  // parity must be correct (even)  
  theRegister = InsertParity(theRegister);   // fix the parity
  
  // compute the address as bytes
  byte addressHighByte = theRegister >> 8;
  byte addressLowByte = theRegister & 0x00FF;

  // assert the chip select (active low)
  digitalWrite(chipSelectPin, LOW);

  // write address to read once for each encoder
  // The return value is garbage the first time through and
  // the data from previous calls on subsequent calls
  // We'll ignore it here because this is only set up once.
  for(int i=0; i<numEncoders; i++)
  {
    // write the address
    SPI.transfer(addressHighByte);
    SPI.transfer(addressLowByte);
  }

  // deassert and reassert chip select once for each
  // group of encoders
  digitalWrite(chipSelectPin, HIGH);
  digitalWrite(chipSelectPin, LOW);

  // read the value of each encoder
  byte resultHigh;
  byte resultLow;
  unsigned short encoderValue;
  for(int i=0; i<numEncoders; i++)
  {
    // read the data
    resultHigh = SPI.transfer(addressHighByte);
    resultLow = SPI.transfer(addressLowByte);
    encoderValue = (resultHigh << 8) | resultLow;
    encoderValue = encoderValue & ENCODER_VALUE_MASK;  // mask to 14 bits

    // Print the angle to the console
    Serial.print("Angle encoder ");
    Serial.print(numEncoders-i);
    Serial.print(": ");
    Serial.print(encoderValue);
    Serial.print("  ");
  }

  digitalWrite(LEDPin, encoderValue != 0 && encoderValue != 16383);
  Serial.print("\n");
  
  // deassert the chip select
  digitalWrite(chipSelectPin, HIGH);
  
}


// Compute and insert the parity bit (even) for communication with the encoder
unsigned short InsertParity(unsigned short theData)
{
  unsigned int value = theData;
  int parity=0;
  for(int i=0; i<16; i++)
  {
    parity = parity ^ (value & 0x0001);
    value = value >> 1;
  }

  if(parity)
      // parity is odd
      theData = theData | 0x8000;  // odd so set the MSB
  else
      // parity is even
      theData = theData & 0x7FFF;   // even so clear the MSB

  return theData;
}







