from TakkTile import TakkTile
import numpy as np

def test(indices, noise_thresh=1, data_pts=100):

	INDICES = indices
	NOISE_THRESH = noise_thresh
	DATA_PTS = data_pts

	success = True

	tk = TakkTile()
	print '\nsensors:', tk.alive
	for i in INDICES:
		if not i in tk.alive:
			print 'FAIL: missing sensor %i'%i
			success = False

	if success:	
		data = np.zeros([DATA_PTS, len(INDICES)])

		tk.startSampling()
		for i in range(DATA_PTS):
			dd = tk.getDataRaw()
			data[i] = [dd[j][0] for j in INDICES]

		noise = dict(zip(INDICES, np.std(data, 0)))
		print noise
		for i in INDICES:
			if not noise[i] < NOISE_THRESH:
				print 'FAIL: sensor %i is noisy: %f'%(i, noise[i])
				success = False
		print 'noise:', [noise[i] for i in INDICES]

	if success:
		print '\nA-OK\n'
	else:
		print '\nFAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL FAIL\n'

	tk.stopSampling()

